package com.example.manufaktur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ManufakturApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManufakturApplication.class, args);
	}

}
