package com.example.manufaktur.controller;

import com.example.manufaktur.exception.ResourceNotFoundException;
import com.example.manufaktur.model.BarangModel;
import com.example.manufaktur.repository.BarangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class BarangController {
    @Autowired
    private BarangRepository barangRepository;

    @GetMapping("/barang")
    public Page<BarangModel> getBarang(Pageable pageable) {
        return barangRepository.findAll(pageable);
    }

    @PostMapping("/barang")
    public BarangModel createBarang(@Valid @RequestBody BarangModel barang) {
        return barangRepository.save(barang);
    }

    @PutMapping("/barang/{barangId}")
    public BarangModel updateBarang(@PathVariable Long barangId,
                                    @Valid @RequestBody BarangModel barangReq) {
        return barangRepository.findById(barangId)
                .map(barangModel -> {
                    barangModel.setNama(barangReq.getNama());
                    barangModel.setDeskripsi(barangReq.getDeskripsi());
                    return barangRepository.save(barangModel);
                }).orElseThrow(() -> new ResourceNotFoundException("Barang kaga ada"));
    }

    @DeleteMapping("/barang/{barangId}")
    public ResponseEntity<?> deleteBarang(@PathVariable Long barangId) {
        return barangRepository.findById(barangId)
                .map(barangModel -> {
                    barangRepository.delete(barangModel);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("barang kaga ada"));
    }
}
