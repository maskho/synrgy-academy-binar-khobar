package com.example.manufaktur.controller;

import com.example.manufaktur.exception.ResourceNotFoundException;
import com.example.manufaktur.model.PelangganModel;
import com.example.manufaktur.repository.BarangRepository;
import com.example.manufaktur.repository.PelangganRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PelangganController {
    @Autowired
    private PelangganRepository pelangganRepository;
    @Autowired
    private BarangRepository barangRepository;

    @GetMapping("/barang/{barangId}/pelanggan")
    public List<PelangganModel> getPelangganByBarangId(@PathVariable Long barangId) {
        return pelangganRepository.findByBarangId(barangId);
    }

    @PostMapping("/barang/{barangId}/pelanggan")
    public PelangganModel addPelanggan(@PathVariable Long barangId,
                                       @Valid @RequestBody PelangganModel pelanggan) {
        return barangRepository.findById(barangId)
                .map(barang -> {
                    pelanggan.setBarang(barang);
                    return pelangganRepository.save(pelanggan);
                }).orElseThrow(() -> new ResourceNotFoundException("Barang kaga ada"));
    }

    @PutMapping("barang/{barangId}/pelanggan/{pelangganId}")
    public PelangganModel updatePelanggan(@PathVariable Long barangId,
                                          @PathVariable Long pelangganId,
                                          @Valid @RequestBody PelangganModel pelangganReq) {
        if (!barangRepository.existsById(barangId)) {
            throw new ResourceNotFoundException("Barang kaga ada");
        }
        return pelangganRepository.findById(pelangganId)
                .map(pelanggan -> {
                    pelanggan.setNama(pelangganReq.getNama());
                    pelanggan.setAlamat(pelangganReq.getAlamat());
                    return pelangganRepository.save(pelanggan);
                }).orElseThrow(() -> new ResourceNotFoundException("Pelanggan kaga ada"));
    }

    @DeleteMapping("/barang/{barangId}/pelanggan/{pelangganId}")
    public ResponseEntity<?> deletePelanggan(@PathVariable Long barangId,
                                             @PathVariable Long pelangganId) {
        if (!barangRepository.existsById(barangId)) {
            throw new ResourceNotFoundException("Barang kaga ada");
        }
        return pelangganRepository.findById(pelangganId)
                .map(pelanggan -> {
                    pelangganRepository.delete(pelanggan);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Data pelanggan kaga ada"));
    }
}
