package com.example.manufaktur.model;
//kelas abstrak untuk field yang memiliki kesamaan di data pelanggan dan data barang
//eg. waktu dibuat dan diperbarui

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"dibuat", "diperbarui"},
        allowGetters = true
)
@Data
public abstract class AuditModel implements Serializable {
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dibuat_pada", nullable = false, updatable = false)
    @CreatedDate
    private Date dibuat;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "diperbarui_pada", nullable = false)
    @LastModifiedDate
    private Date diperbarui;

//    public Date getDibuat() {
//        return dibuat;
//    }
//
//    public void setDibuat(Date dibuat) {
//        this.dibuat = dibuat;
//    }
//
//    public Date getDiperbarui() {
//        return diperbarui;
//    }
//
//    public void setDiperbarui(Date diperbarui) {
//        this.diperbarui = diperbarui;
//    }
}
