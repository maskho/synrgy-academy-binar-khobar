package com.example.manufaktur.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "barang")
@Data
public class BarangModel extends AuditModel {
    @Id
    @GeneratedValue(generator = "generator_barang")
    @SequenceGenerator(
            name = "generator_barang",
            sequenceName = "sequence_barang",
            initialValue = 1000
    )
    private Long id;

    @NotBlank
    @Size(min = 3, max = 30)
    private String nama;

    @Column(columnDefinition = "text")
    private String deskripsi;

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getNama() {
//        return nama;
//    }
//
//    public void setNama(String nama) {
//        this.nama = nama;
//    }
//
//    public String getDeskripsi() {
//        return deskripsi;
//    }
//
//    public void setDeskripsi(String deskripsi) {
//        this.deskripsi = deskripsi;
//    }
}
