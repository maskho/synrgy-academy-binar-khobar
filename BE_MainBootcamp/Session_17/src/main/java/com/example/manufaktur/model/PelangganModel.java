package com.example.manufaktur.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "pelanggan")
@Data
public class PelangganModel extends AuditModel {
    @Id
    @GeneratedValue(generator = "generator_pelanggan")
    @SequenceGenerator(
            name = "generator_pelanggan",
            sequenceName = "sequence_pelanggan",
            initialValue = 5000
    )
    private Long id;

    @Column(columnDefinition = "text")
    private String nama;
    private String alamat;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "barang_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private BarangModel barang;

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getNama() {
//        return nama;
//    }
//
//    public void setNama(String nama) {
//        this.nama = nama;
//    }
//
//    public String getAlamat() {
//        return alamat;
//    }
//
//    public void setAlamat(String alamat) {
//        this.alamat = alamat;
//    }
//
//    public BarangModel getBarang() {
//        return barang;
//    }
//
//    public void setBarang(BarangModel barang) {
//        this.barang = barang;
//    }
}
