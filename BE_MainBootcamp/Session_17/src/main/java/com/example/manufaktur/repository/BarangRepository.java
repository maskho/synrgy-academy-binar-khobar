package com.example.manufaktur.repository;

import com.example.manufaktur.model.BarangModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarangRepository extends JpaRepository<BarangModel,Long> {
}
