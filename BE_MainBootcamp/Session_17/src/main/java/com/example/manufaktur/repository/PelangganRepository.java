package com.example.manufaktur.repository;

import com.example.manufaktur.model.PelangganModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PelangganRepository extends JpaRepository<PelangganModel, Long> {
    List<PelangganModel> findByBarangId(Long barangId);
}
