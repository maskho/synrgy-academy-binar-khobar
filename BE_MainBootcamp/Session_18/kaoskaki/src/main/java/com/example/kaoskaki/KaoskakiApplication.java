package com.example.kaoskaki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaoskakiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KaoskakiApplication.class, args);
	}

}
