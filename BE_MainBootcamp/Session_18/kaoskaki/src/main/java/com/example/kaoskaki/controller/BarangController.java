package com.example.kaoskaki.controller;

import com.example.kaoskaki.exception.DataNotFoundException;
import com.example.kaoskaki.model.Barang;
import com.example.kaoskaki.model.Kategori;
import com.example.kaoskaki.repository.BarangRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BarangController {
    @Autowired
    private BarangRepo barangRepo;

    //GET semua barang
    @GetMapping("/barang/all")
    public Page<Barang> getBarang(Pageable pageable) {
        return barangRepo.findAll(pageable);
    }

    //GET pake query parameter /barang?barang={nama_barang}
    @GetMapping("/barang")
    public List<Barang> getBarang(String nama) {
        if(barangRepo.findByNama(nama).isEmpty()){
            throw new DataNotFoundException(nama);
        }
        return barangRepo.findByNama(nama);
    }

    @PostMapping("/barang")
    public Barang createBarang(@Valid @RequestBody Barang barang) {
        return barangRepo.save(barang);
    }

    @PutMapping("/barang/{barangId}")
    public Barang updateBarang(@PathVariable Long barangId,
                               @Valid @RequestBody Barang barangReq) {
        return barangRepo.findById(barangId)
                .map(barang -> {
                    barang.setNama(barangReq.getNama());
                    barang.setUkuran(barangReq.getUkuran());
                    barang.setHarga(barangReq.getHarga());
                    return barangRepo.save(barang);
                }).orElseThrow(() -> new DataNotFoundException(barangId));
    }

    @DeleteMapping("/barang/{barangId}")
    public ResponseEntity<?> deleteBarang(@PathVariable Long barangId) {
        return barangRepo.findById(barangId)
                .map(barang -> {
                    barangRepo.delete((barang));
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new DataNotFoundException(barangId));
    }

}
