package com.example.kaoskaki.controller;

import com.example.kaoskaki.exception.DataNotFoundException;
import com.example.kaoskaki.model.Kategori;
import com.example.kaoskaki.repository.KategoriRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class KategoriController {

    @Autowired
    private KategoriRepo kategoriRepo;

    //GET semua kategori
    @GetMapping("/kategori/all")
    public Page<Kategori> getKategori(Pageable pageable) {
        return kategoriRepo.findAll(pageable);
    }

    //GET pake query parameter /kategori?kategori={judul_kategori}
    @GetMapping("/kategori")
    public List<Kategori> getKategori(String kategori) {
        if(kategoriRepo.findByKategori(kategori).isEmpty()){
            throw new DataNotFoundException(kategori);
        }
        return kategoriRepo.findByKategori(kategori);
    }

    @PostMapping("/kategori")
    public Kategori createKategori(@Valid @RequestBody Kategori kategori) {
        return kategoriRepo.save(kategori);
    }

    @PutMapping("/kategori/{kategoriId}")
    public Kategori updateKategori(@PathVariable Long kategoriId,
                                   @Valid @RequestBody Kategori kategoriReq) {
        return kategoriRepo.findById(kategoriId)
                .map(kategori -> {
                    kategori.setKategori(kategoriReq.getKategori());
                    kategori.setDeskripsi(kategoriReq.getDeskripsi());
                    return kategoriRepo.save(kategori);
                }).orElseThrow(() -> new DataNotFoundException(kategoriId));
    }

    @DeleteMapping("/kategori/{kategoriId}")
    public ResponseEntity<?> deleteKategori(@PathVariable Long kategoriId) {
        return kategoriRepo.findById(kategoriId)
                .map(kategori -> {
                    kategoriRepo.delete(kategori);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new DataNotFoundException(kategoriId));
    }
}
