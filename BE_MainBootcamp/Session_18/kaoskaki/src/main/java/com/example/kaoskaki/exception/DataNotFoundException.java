package com.example.kaoskaki.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class DataNotFoundException extends RuntimeException{
    public DataNotFoundException(Long id){
        super("Yang kamu cari di id: "+id+" kaga ada");
    }
    public DataNotFoundException(String nama){
        super("data "+nama+" yang kamu cari kaga ada");
    }
}
