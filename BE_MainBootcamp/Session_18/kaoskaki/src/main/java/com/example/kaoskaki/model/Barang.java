package com.example.kaoskaki.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "kaoskaki")
@Data
public class Barang {
    @Id@GeneratedValue(generator = "generator_kaoskaki")
    @SequenceGenerator(
            name = "generator_kaoskaki",
            sequenceName = "sequence_kaoskaki",
            initialValue = 4000
    )
    private Long id;

    @NotBlank
    @Size(min = 3, max = 30)
    private String nama;

    @Column(columnDefinition = "text")
    private String ukuran;
    private String harga;
}
