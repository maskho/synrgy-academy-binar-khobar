package com.example.kaoskaki.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "kategori")
@Data
public class Kategori {
    @Id
    @GeneratedValue(generator = "generator_kategori")
    @SequenceGenerator(
            name = "generator_kategori",
            sequenceName = "sequence_kategori",
            initialValue = 2000
    )
    private Long id;

    @NotBlank
    @Size(min=3,max=20)
    private String kategori;

    @Column(columnDefinition = "text")
    private String deskripsi;
}
