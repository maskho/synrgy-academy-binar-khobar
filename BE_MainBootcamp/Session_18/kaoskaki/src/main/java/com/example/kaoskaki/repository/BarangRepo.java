package com.example.kaoskaki.repository;

import com.example.kaoskaki.model.Barang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarangRepo extends JpaRepository<Barang,Long> {
    List<Barang> findByNama(String nama);
}
