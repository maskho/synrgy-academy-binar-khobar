package com.example.kaoskaki.repository;

import com.example.kaoskaki.model.Kategori;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

@Repository
public interface KategoriRepo extends JpaRepository<Kategori,Long> {
List<Kategori> findByKategori(String kategori);
}
