package com.synrgy;
//Tugas Sesi 4
//Tanpa bikin looping

import java.util.Arrays;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
	    int[] mat = {70,72,81,85,90};
	    int[] bi = {75,78,79,81,80};
	    int[] ipa = {78,80,78,80,76};

	    int[] jones_jomblo = {mat[0],bi[0],ipa[0]};
	    int[] dimas_anggara = {mat[1],bi[1],ipa[1]};
	    int[] janu_jalu = {mat[2],bi[2],ipa[2]};
	    int[] galantis_f = {mat[3],bi[3],ipa[3]};
	    int[] ferdinan_m = {mat[4],bi[4],ipa[4]};

        System.out.println("Nilai total Jones Jomblo: "+(jones_jomblo[0]+jones_jomblo[1]+jones_jomblo[2]));
        System.out.println("Nilai total Dimas Anggara: "+(dimas_anggara[0]+dimas_anggara[1]+dimas_anggara[2]));
        System.out.println("Nilai total Janu Jalu: "+(janu_jalu[0]+janu_jalu[1]+janu_jalu[2]));
        System.out.println("Nilai total Galantis F.: "+(galantis_f[0]+galantis_f[1]+galantis_f[2]));
        System.out.println("Nilai total Ferdinan M.: "+(ferdinan_m[0]+ferdinan_m[1]+ferdinan_m[2]));

        System.out.println("\nRata-rata matematika: "+Double.valueOf(mat[0]+mat[1]+mat[2]+mat[3]+mat[4])/5);
        System.out.println("Rata-rata bahasa Indonesia: "+Double.valueOf(bi[0]+bi[1]+bi[2]+bi[3]+bi[4])/5);
        System.out.println("Rata-rata IPA: "+Double.valueOf(ipa[0]+ipa[1]+ipa[2]+ipa[3]+ipa[4])/5);

        Arrays.sort(mat);
        Arrays.sort(bi);
        Arrays.sort(ipa);
        int matTertinggi = mat[mat.length-1];
        int matTerendah = mat[0];
        int biTertinggi = bi[bi.length-1];
        int biTerendah = bi[0];
        int ipaTertinggi = ipa[ipa.length-1];
        int ipaTerendah = ipa[0];

        System.out.print("\nnilai tertinggi matematika: "+matTertinggi+" diraih oleh: ");
        System.out.println((jones_jomblo[0]==matTertinggi) ? "Jones Jomblo" : (dimas_anggara[0]==matTertinggi)?"Dimas Anggara":(janu_jalu[0]==matTertinggi)?"Janu Jalu":(galantis_f[0]==matTertinggi)?"Galantis F.":"Ferdinan M.");
        System.out.print("nilai tertinggi bahasa Indonesia: "+biTertinggi+" diraih oleh: ");
        System.out.println((jones_jomblo[1]==biTertinggi) ? "Jones Jomblo" : (dimas_anggara[1]==biTertinggi)?"Dimas Anggara":(janu_jalu[1]==biTertinggi)?"Janu Jalu":(galantis_f[1]==biTertinggi)?"Galantis F.":"Ferdinan M.");
        System.out.print("nilai tertinggi IPA: "+ipaTertinggi+" diraih oleh: ");
        System.out.println((jones_jomblo[2]==ipaTertinggi) ? "Jones Jomblo" : (dimas_anggara[2]==ipaTertinggi)?"Dimas Anggara":(janu_jalu[2]==ipaTertinggi)?"Janu Jalu":(galantis_f[2]==ipaTertinggi)?"Galantis F.":"Ferdinan M.");

        System.out.print("\nnilai terendah matematika: "+matTerendah+" diraih oleh: ");
        System.out.println((jones_jomblo[0]==matTerendah) ? "Jones Jomblo" : (dimas_anggara[0]==matTerendah)?"Dimas Anggara":(janu_jalu[0]==matTerendah)?"Janu Jalu":(galantis_f[0]==matTerendah)?"Galantis F.":"Ferdinan M.");
        System.out.print("nilai terendah bahasa Indonesia: "+biTerendah+" diraih oleh: ");
        System.out.println((jones_jomblo[1]==biTerendah) ? "Jones Jomblo" : (dimas_anggara[1]==biTerendah)?"Dimas Anggara":(janu_jalu[1]==biTerendah)?"Janu Jalu":(galantis_f[1]==biTerendah)?"Galantis F.":"Ferdinan M.");
        System.out.print("nilai terendah IPA: "+ipaTerendah+" diraih oleh: ");
        System.out.println((jones_jomblo[2]==ipaTerendah) ? "Jones Jomblo" : (dimas_anggara[2]==ipaTerendah)?"Dimas Anggara":(janu_jalu[2]==ipaTerendah)?"Janu Jalu":(galantis_f[2]==ipaTerendah)?"Galantis F.":"Ferdinan M.");
//kelemahan: jika ada 2 atau lebih nilai yang sama maka nama pertama dalam ternary operation diatas yang diambil
    }
}
