package com.synrgy;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[][] database = {{"1", "Peter P", "California", "Male"}, {"2", "Jabukowski R", "California", "Male"}, {"3", "Mark Watney", "Silicon Valley", "Male"}, {"4", "Arina Sanders", "Kanada", "Female"}, {"5", "Matthew T", "New York", "Male"}};
        System.out.println("mencari data matthew");
        for (String[] data : database) {
            for (String datum : data) {
                //jika indexOf menemukan index kata yang dicari
                if (!(datum.indexOf("Matthew")==-1)){
                    System.out.println(Arrays.toString(data));
                }
            }
        }
        System.out.println("\nmencari nama yang bergender laki");
        for (String[] data : database) {
            for (String datum : data) {
                //jika indexOf menemukan index kata yang dicari
                if (!(datum.indexOf("Male")==-1)){
                    System.out.println(data[1]);
                }
            }
        }
        System.out.println("\nmencari nama yang bergender wadon");
        for (String[] data : database) {
            for (String datum : data) {
                //jika indexOf menemukan index kata yang dicari
                if (!(datum.indexOf("Female")==-1)){
                    System.out.println(data[1]);
                }
            }
        }
        System.out.println("\nmenghitung jumlah data");
        System.out.println("Banyak data pada database siswa BCA Academy ada: "+database.length);
        System.out.println("Setiap data ada datum (field) sebanyak:");
        int total =0;
        for(int i=0;i<database.length;i++){
            int fields = database[i].length;
            System.out.println("id"+i+" ada "+fields+ " datum/field");
            total+=fields;
        }
        System.out.println("\nJadi, jumlah datum dalam database siswa BCA Academy sebanyak: "+total);
    }
}