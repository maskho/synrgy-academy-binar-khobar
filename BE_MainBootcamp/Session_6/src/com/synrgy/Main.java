package com.synrgy;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Modif kalimat = new Modif();
        kalimat.setKalimat("Aku belajar java bersama BCA Academy");
        System.out.println("kalo dibalik semua: "+kalimat.balikSemua());
        System.out.println("kalo dibalik kata aja: "+kalimat.balikKata());
        System.out.println("kalo dikapitalkan: "+kalimat.besar());
        System.out.println("kalo dikecilkan: "+kalimat.kecil());
        System.out.println("jumlah karakter ada: "+kalimat.jmlKarakter());
        System.out.println("ditambah kata jadi: "+kalimat.tambahKata("X Binar Academy"));
    }
}
