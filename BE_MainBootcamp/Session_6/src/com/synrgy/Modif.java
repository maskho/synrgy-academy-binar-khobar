package com.synrgy;

public class Modif {
    private String kalimat;

    public void setKalimat(String kalimat) {
        this.kalimat = kalimat;
    }
    public String balikSemua(){
        char[] kebalik = new char[kalimat.length()];
        int i=kalimat.length()-1;
        //mengambil karakter dalam string dan membalikkannya
        for (char huruf: kalimat.toCharArray()){
            kebalik[i]=huruf;
            i--;
        }
        //mengembalikan hasil berupa string dengan mengubah array char -> String
        return String.valueOf(kebalik);
    }
    public String balikKata(){
        //memisahkan tiap kata menjadi array
        String[] katakata = kalimat.split(" ");
        String[] kebalik = new String[katakata.length];
        String hasil="";
        int i=katakata.length-1;
        //membalik array
        for (String kata: katakata){
            kebalik[i]=kata;
            i--;
        }
        //menggabungkan array string menjadi String kalimat baru
        for(String kata: kebalik){
            hasil +=kata+" ";
        }
        return hasil;
    }
    public String besar(){
        return kalimat.toUpperCase();
    }
    public String kecil(){
        return kalimat.toLowerCase();
    }
    public int jmlKarakter(){
        return kalimat.length();
    }
    public String tambahKata(String tambah){
        return kalimat+" "+tambah;
    }
}
