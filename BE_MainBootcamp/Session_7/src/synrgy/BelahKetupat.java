package synrgy;

public class BelahKetupat extends BangunDatar {
    private double d1, d2, miring;


    public BelahKetupat(double d1, double d2) {
        this.d1 = d1;
        this.d2 = d2;
    }
    public BelahKetupat() {
        System.out.println("jangan lupa set d1 & d2");
    }


    public void setD1(double d1) {
        this.d1 = d1;
    }

    public void setD2(double d2) {
        this.d2 = d2;
    }

    @Override
    double luas() {
        return 1 / 2 * d1 * d2;
    }

    @Override
    double keliling() {
        miring = Math.sqrt(Math.pow(1 / 2 * d1, 2) + Math.pow(1 / 2 * d2, 2));
        return 4 * miring;
    }
}
