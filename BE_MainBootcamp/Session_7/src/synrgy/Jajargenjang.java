package synrgy;

public class Jajargenjang extends BangunDatar {
    private double alas, tinggi, miring;

    public Jajargenjang() {
        System.out.println("Masukkan alas, tinggi, miring");
    }

    public Jajargenjang(double alas, double tinggi, double miring) {
        this.alas = alas;
        this.tinggi = tinggi;
        this.miring = miring;
    }

    public void setAlas(double alas) {
        this.alas = alas;
    }

    public void setTinggi(double tinggi) {
        this.tinggi = tinggi;
    }

    public void setMiring(double miring) {
        this.miring = miring;
    }

    @Override
    double luas() {
        return alas * tinggi;
    }

    @Override
    double keliling() {
        return 2 * miring + alas + tinggi;
    }
}
