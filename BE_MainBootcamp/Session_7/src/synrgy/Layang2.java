package synrgy;

public class Layang2 extends BangunDatar {
    private double d1, d2, m1, m2;

    public Layang2() {
        System.out.println("masukkan diagonal 1, diagonal 2, miring 1, miring 2");
    }

    public Layang2(double d1, double d2, double m1, double m2) {
        this.d1 = d1;
        this.d2 = d2;
        this.m2 = m2;
        this.m1 = m1;
    }

    public void setD2(double d2) {
        this.d2 = d2;
    }

    public void setD1(double d1) {
        this.d1 = d1;
    }

    public void setM1(double m1) {
        this.m1 = m1;
    }

    public void setM2(double m2) {
        this.m2 = m2;
    }

    @Override
    double luas() {
        return 1 / 2 * d1 * d2;
    }

    @Override
    double keliling() {
        return 2 * (m1 + m2);
    }
}
