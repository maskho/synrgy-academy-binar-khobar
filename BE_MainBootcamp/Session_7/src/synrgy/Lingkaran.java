package synrgy;

public class Lingkaran extends BangunDatar {
    private double radius;

    public Lingkaran(double radius) {
        this.radius = radius;
    }

    public Lingkaran() {
        System.out.println("jangan lupa set radius");
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    double luas() {
        return radius * radius * Math.PI;
    }

    @Override
    double keliling() {
        return 2 * radius * Math.PI;
    }
}
