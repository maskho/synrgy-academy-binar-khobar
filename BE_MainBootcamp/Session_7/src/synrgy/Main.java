package synrgy;

public class Main {

    public static void main(String[] args) {
        System.out.println("Kalkulator Bangun Ruang");
        Lingkaran bunder = new Lingkaran(3);
        System.out.println("luas bunder adalah " + bunder.luas());
        System.out.println("keliling bunder adalah " + bunder.keliling());

        Trapesium trapesium = new Trapesium(2, 4, 2, 3, 3);
        BelahKetupat belahKetupat = new BelahKetupat(4, 6);
        Layang2 layang2 = new Layang2(4, 8, 5, 10);
        Persegi persegi = new Persegi(10);
        Segitiga segitiga = new Segitiga(3, 4, 5, 5);
        PersegiPanjang persegiPanjang = new PersegiPanjang();
        persegiPanjang.setAlas(10);
        persegiPanjang.setTinggi(5);
        System.out.println("luas persegi panjang " + persegiPanjang.luas());
        System.out.println("keliling persegi panjang " + persegiPanjang.keliling());
    }
}
