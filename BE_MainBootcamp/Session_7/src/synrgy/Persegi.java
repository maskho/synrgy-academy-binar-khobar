package synrgy;

public class Persegi extends BangunDatar{
    private double sisi;
    public Persegi(){
        System.out.println("jangan lupa masukkan sisi");
    }
    public Persegi(double sisi){
        this.sisi=sisi;
    }

    public void setSisi(double sisi) {
        this.sisi = sisi;
    }

    @Override
    double keliling() {
        return 4*sisi;
    }

    @Override
    double luas() {
        return sisi*sisi;
    }
}
