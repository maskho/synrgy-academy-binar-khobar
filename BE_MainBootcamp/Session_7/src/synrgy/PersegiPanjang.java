package synrgy;

public class PersegiPanjang extends BangunDatar {
    private double alas, tinggi;

    public PersegiPanjang() {
        System.out.println("janlup masukkan alas dan tinggi");
    }

    public PersegiPanjang(double alas, double tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;
    }

    public void setTinggi(double tinggi) {
        this.tinggi = tinggi;
    }

    public void setAlas(double alas) {
        this.alas = alas;
    }

    @Override
    double keliling() {
        return 2 * (alas + tinggi);
    }

    @Override
    double luas() {
        return alas * tinggi;
    }
}
