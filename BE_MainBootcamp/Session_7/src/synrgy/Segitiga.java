package synrgy;

public class Segitiga extends BangunDatar{
    private double alas,tinggi, miring, miring2;
    public Segitiga(){
        System.out.println("jangan lupa set alas, tinggi, miring");
    }

    public void setAlas(double alas) {
        this.alas = alas;
    }

    public void setTinggi(double tinggi) {
        this.tinggi = tinggi;
    }

    public void setMiring(double miring) {
        this.miring = miring;
    }

    public void setMiring2(double miring2) {
        this.miring2 = miring2;
    }
    public Segitiga(double alas,double tinggi,double miring,double miring2){
        this.alas=alas;
        this.tinggi=tinggi;
        this.miring=miring;
        this.miring2=miring2;
    }

    @Override
    double luas() {
        return 1/2*alas*tinggi;
    }

    @Override
    double keliling() {
        return alas+miring+miring2;
    }
}
