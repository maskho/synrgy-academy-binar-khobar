package synrgy;

public class Trapesium extends BangunDatar {
    private double a, b, t, m1, m2;


    public Trapesium(double a, double b, double t, double m1, double m2) {
        this.a = a;
        this.b = b;
        this.t = t;
        this.m1 = m1;
        this.m2 = m2;
    }

    public Trapesium() {
        System.out.println("masukkan nilai a,b, tinggi, miring");
    }
    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setT(double t) {
        this.t = t;
    }

    public void setM1(double m1) {
        this.m1 = m1;
    }

    public void setM2(double m2) {
        this.m2 = m2;
    }

    @Override
    double luas() {
        return 1 / 2 * ((a + b) * t);
    }

    @Override
    double keliling() {
        return a + b + m1 + m2;
    }
}
