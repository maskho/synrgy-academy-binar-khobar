import React from "react";
import "./App.css";
import { Route, Switch } from "react-router-dom";
import Login from "./components/Login";
import Coblos from "./components/Coblos";
import Hasil from "./components/Hasil";

function App() {
  return (
    //membuat routing aplikasi
    <div className="App">
      <Switch>
        <header className="App-header">
          <Route path="/" exact={true}>
            <Login />
          </Route>
          <Route path="/coblos" exact={true}>
            <Coblos />
          </Route>
          <Route path="/hasil" exact={true}>
            <Hasil />
          </Route>
        </header>
      </Switch>
    </div>
  );
}

export default App;
