import React, { useState } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Figure from "react-bootstrap/Figure";

function Coblos(props) {
  //mengatur state dari input
  const [state, setState] = useState({
    pilihan: "",
    successMessage: null,
  });
  //menangani perubahan state
  const handleChange = (e) => {
    const { id, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };
  //menangani submit data
  const handleSubmitClick = (e) => {
    e.preventDefault();
    //nama harus diisi

    const payload = {
      pilihan: state.pilihan,
    };
    //mengirim data ke backend
    axios
      .post("http://localhost:4000/user", payload)
      .then(function (response) {
        if (response.data.code === 200) {
          setState((prevState) => ({
            ...prevState,
            successMessage: "Data terkirim",
          }));
          //menuju halaman hasil
          redirectToHasil();
          props.showError(null);
        } else {
          props.showError("Terdapat kesalahan");
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const redirectToHasil = () => {
    props.history.push("/hasil");
  };
  return (
    <Jumbotron>
      <h1>Pilihlah sesuai hati nuranimu</h1>
      <p>Pilihan anda menentukan masa depan kelas kita</p>
      <Container>
        <Row>
          <Col xs={6} md={4}>
            <Figure.Image src={require("./img/ashiap.png")} thumbnail />
            <Figure.Caption>Yogi Ashiap </Figure.Caption>
            <Button variant="dark" value="1" onChange={handleChange}>
              Pilih
            </Button>
          </Col>
          <Col xs={6} md={4}>
            <Figure.Image src={require("./img/corbuzzer.png")} thumbnail />
            <Figure.Caption>Tata Corbuzzer </Figure.Caption>
            <Button variant="dark" value="2" onChange={handleChange}>
              Pilih
            </Button>
          </Col>
          <Col xs={6} md={4}>
            <Figure.Image src={require("./img/dika.png")} thumbnail />
            <Figure.Caption>Reja Dika</Figure.Caption>
            <Button variant="dark" value="3" onChange={handleChange}>
              Pilih
            </Button>
          </Col>
        </Row>
      </Container>
      <br></br>
      <Button variant="primary" size="lg" block onClick={handleSubmitClick}>
        Kirim
      </Button>
    </Jumbotron>
  );
}
export default withRouter(Coblos);
