import React from "react";
import axios from "axios";
import Table from "react-bootstrap/Table";
import Jumbotron from "react-bootstrap/Jumbotron";

export default class Hasil extends React.Component {
  state = {
    persons: [],
  };

  componentDidMount() {
    axios.get(`http://localhost:4000/users`).then((res) => {
      const pilihan = res.data;
      this.setState({ pilihan });
    });
  }
  render() {
    return (
      <Jumbotron>
        <h1>Hasil Polling Ketua Kelas XIII IPS SMA Binar</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Nama </th>
              <th>Hasil</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Yogi</td>
              <td>hasil</td>
            </tr>
            <tr>
              <td>Tata</td>
              <td>hasil</td>
            </tr>
            <tr>
              <td>Reja</td>
              <td>hasil</td>
            </tr>
          </tbody>
        </Table>
      </Jumbotron>
    );
  }
}
