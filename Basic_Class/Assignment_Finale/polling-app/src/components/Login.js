import React, { useState } from "react";
import axios from "axios";
import Jumbotron from "react-bootstrap/Jumbotron";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { withRouter } from "react-router-dom";

function Login(props) {
  //mengatur state dari input
  const [state, setState] = useState({
    nama: "",
    successMessage: null,
  });
  //menangani perubahan state
  const handleChange = (e) => {
    const { id, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };
  //menangani ketika submit data
  const handleSubmitClick = (e) => {
    e.preventDefault();
    //mengisi data
    if (state.nama.length) {
      const payload = {
        nama: state.nama,
      };
      //mengirim data ke backend
      axios
        .post("http://localhost:4000/user", payload)
        .then(function (response) {
          if (response.data.code === 200) {
            setState((prevState) => ({
              ...prevState,
              successMessage: "Data terkirim",
            }));
            //menuju halaman Coblos
            redirectToCoblos();
            //props.showError(null);
          } else {
            //props.showError("Terdapat kesalahan");
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      //props.showError("Harap isikan nama anda");
    }
  };
  const redirectToCoblos = () => {
    props.history.push("/coblos");
  };
  return (
    <Jumbotron>
      <h1>Polling Ketua Kelas XIII IPS SMA Binar</h1>
      <p>Silahkan registrasi nama anda agar dapat mengikuti polling ini </p>
      <Form>
        <Form.Group controlId="nama">
          <Form.Control
            type="text"
            placeholder="Masukkan nama anda"
            value={state.nama}
            onChange={handleChange}
          />
          <Form.Text className="text-muted">
            Kami tidak akan memberitahu pilihan anda kepada orang lain
          </Form.Text>
        </Form.Group>
        <Button variant="primary" onClick={handleSubmitClick}>
          Daftar
        </Button>
      </Form>
    </Jumbotron>
  );
}
export default withRouter(Login);
