const mongoose = require("mongoose");
//membuat model document
const UserSchema = new mongoose.Schema({
  nama: { type: String },
  pilihan: { type: String },
});

module.exports = mongoose.model("User", UserSchema);
