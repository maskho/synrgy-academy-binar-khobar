const express = require("express");
const userModel = require("../models/User.Model");
const app = express();
//mengambil semua data polling
app.get("/users", async (req, res) => {
  const user = await userModel.find({});

  try {
    res.send(user);
  } catch (err) {
    res.status(500).send(err);
  }
});
//mengambil data polling sesuai id
app.get("/user/:id", async (req, res) => {
  try {
    const users = await userModel.findById(req.params.id, req.body);
    //jika tidak ada id yang dimaksud, menampilkan pesan
    if (!user) res.status(404).send("Tidak ada id tersebut");
    res.send(users);
  } catch (err) {
    res.status(500).send(err);
  }
});
//mengirim data ke database
app.post("/user", async (req, res) => {
  const user = new userModel(req.body);

  try {
    //menyimpan data di database
    await user.save();
    res.send(user);
  } catch (err) {
    res.status(500).send(err);
  }
});
//melakukan update data sesuai id
app.patch("/user/:id", async (req, res) => {
  try {
    await userModel.findByIdAndUpdate(req.params.id, req.body);
    await userModel.save();
    res.send(user);
  } catch (err) {
    res.status(500).send(err);
  }
});
//menghapus data sesuai id
app.delete("/user/id", async (req, res) => {
  try {
    const user = await userModel.findByIdAndDelete(req.params.id);

    if (!user) res.status(404).send("Tidak ada id tersebut");
    res.status(200).send;
  } catch (err) {
    res.status(500).send(err);
  }
});
module.exports = app;
