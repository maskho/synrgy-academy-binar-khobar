const express = require("express");
const mongoose = require("mongoose");
bodyParser = require("body-parser");
const userRouter = require("./routes/User.Route.js");
const cors = require("cors");
const app = express();
//setting cors
app.use(cors());
app.use(express.json());

mongoose.connect(
  //database polling
  "mongodb+srv://waKwaw97:waKwaw97@cluster0-dnxx2.gcp.mongodb.net/polling?retryWrites=true&w=majority",
  {
    useUnifiedTopology: true,
    //body-parser sudah deprecated, pake ini
    useNewUrlParser: true,
  }
);

app.use(userRouter);

//menentukan port yang digunakan
let port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log("server gas on port:" + port);
});
