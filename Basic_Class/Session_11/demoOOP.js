//definisi class
class eMoneyAccount {
    //definisi constructor
    //asumsi: user mendaftar dengan nama & telp, dan saldo awal 0
    constructor(nameInit, phoneInit) {
      this.name = nameInit;
      this.phone = phoneInit;
      this.amount = 0;
    }
    //ketika topup, saldo akan bertambah sesuai jumlah yang di topup
    topUp(value) {
      this.amount += value;
    }
    //membelanjakan sejumlah value
    spend(value) {
      this.amount -= value;
    }
    //recipient adalah object eMoney Account lain
    //value adalah jumlah transfer
    transfer(recipient, value) {
      recipient.amount += value;
      this.amount -= value;
    }
    //setter atribut nama dan no hp
    setname(newName) {
      this.name = newName;
    }
  
    setPhone(newPhone) {
      this.phone = newPhone;
    }
}
var dataNasabah = [];
var counter = 0;

function simpanData(nama, no){
  var dataBaru = new eMoneyAccount(nama,no);
  dataNasabah.push(dataBaru);
  tambahSelection(nama,"akun");
  tambahSelection(nama,"transfer_option")
}
document.getElementById("bt_akun").onclick = function(){
  simpanData(document.getElementById("input_nama").value, document.getElementById("input_no").value);
}

function tambahSelection(nama,id){
  var option = document.createElement("option");
  option.text = nama;
  counter++;
  document.getElementById(id).add(option);
}

function tampilkanData(){
  var x = dataNasabah[document.getElementById("akun").selectedIndex]
  document.getElementById("hasil_nama").innerHTML = "Nama Nasabah: "+x.name;
  document.getElementById("hasil_no").innerHTML = "No. Hp Nasabah: "+x.phone;
  document.getElementById("hasil_amount").innerHTML = "Saldo Nasabah: "+x.amount;

  document.getElementById("topup").setAttribute("placeholder", "top up saldo "+x.name);
  document.getElementById("spend").setAttribute("placeholder", "cairkan saldo "+x.name);
  document.getElementById("transfer_saldo").setAttribute("placeholder", "transfer saldo "+x.name)

  tujuanTransfer();
}
function tambah(){
  var duit = parseInt(document.getElementById("topup").value);
  try{
    if(duit<0)throw "isian tidak boleh minus";
    if(isNaN(duit)) throw "isian harus angka";
  }catch(err){
    alert("TRANSAKSI GAGAL\n"+err);
    duit = 0;
  }
  dataNasabah[document.getElementById("akun").selectedIndex].topUp(duit);
  tampilkanData();
}
function cair(){
  var duit = parseInt(document.getElementById("spend").value);
  try{
    if(duit<0)throw "isian tidak boleh minus";
    if(isNaN(duit)) throw "isian harus angka";
    if(duit>dataNasabah[document.getElementById("akun").selectedIndex].amount) throw "saldo tidak mencukupi";
  }catch(err){
    alert("TRANSAKSI GAGAL\n"+err);
    duit = 0;
  }
  dataNasabah[document.getElementById("akun").selectedIndex].spend(duit);
  tampilkanData();
}
function transferDuit(){
  var duit = parseInt(document.getElementById("transfer_saldo").value);
  try{
    if(duit<0)throw "isian tidak boleh minus";
    if(isNaN(duit)) throw "isian harus angka";
    if(duit>dataNasabah[document.getElementById("akun").selectedIndex].amount) throw "saldo tidak mencukupi";
  }catch(err){
    alert("TRANSAKSI GAGAL\n"+err);
    duit = 0;
  }
  dataNasabah[document.getElementById("akun").selectedIndex].transfer(dataNasabah[document.getElementById("transfer_option").selectedIndex], parseInt(document.getElementById("transfer_saldo").value));
  tampilkanData();
}
  //instansiasi: membentuk objek riil dari class
  // var Dwi = new eMoneyAccount("Dwi","0808080");
  // console.log("saldo Dwi: "+Dwi.amount);
  // var Abrar = new eMoneyAccount("Abrar","12345");
  // console.log("saldo Abrar: "+Abrar.amount);
  // console.log("==========");
  // console.log("Dwi top up Rp300.000");
  // Dwi.topUp(300000);
  // console.log("saldo Dwi: "+Dwi.amount);
  // console.log("==========");
  // console.log("Dwi transfer Rp80.000 ke Abrar");
  // Dwi.transfer(Abrar, 80000);
  // console.log("saldo Dwi: "+Dwi.amount);
  // console.log("saldo Abrar: "+Abrar.amount);
  
  //tampilkan informasi akun emoney Dwi & Abrar
  // document.getElementById('dwi').children[0].innerHTML = "nama: "+Dwi.name;
  // document.getElementById('dwi').children[1].innerHTML = "no. hp: "+Dwi.phone;
  // document.getElementById('dwi').children[2].innerHTML = "saldo: "+Dwi.amount;

  // document.getElementById('abrar').children[0].innerHTML = "nama: "+Abrar.name;
  // document.getElementById('abrar').children[1].innerHTML = "no. hp: "+Abrar.phone;
  // document.getElementById('abrar').children[2].innerHTML = "saldo: "+Abrar.amount;

//ambil lirik dari API
// function fetchLyrics(artist, title) {
//   const Http = new XMLHttpRequest();
//   var url = 'https://api.lyrics.ovh/v1/'+artist+'/'+title;
//   var output = document.getElementById("lyricoutput");
//   Http.open("GET", url); //kirim HTTP request dengan metode GET
//   Http.send();

//   //ketika balasan dari server sudah ada
//   Http.onreadystatechange = function() {
//       if(this.readyState==4) {
//       var result = JSON.parse(Http.responseText);
//       output.innerHTML = result.lyrics;
//       }
//   }
// }
