console.log("TUGAS NO 2");

//create new file
var fs = require('fs');
fs.writeFile('novel.txt', '', function(err){
    if(err) throw err;
    console.log('file write success');
});

//update
fs.appendFile('novel.txt', 'Kuda laut, hewan yang sangat eksotis. Memiliki keunikan yang sangat luar biasa',function(err){
    if(err) throw err;
    console.log('updated');
});

//read file
fs.readFile('novel.txt', function(err, data){
    if(err) throw err;
    console.log('data: '+data);
});

//delete file
// fs.unlink('novel.txt', function(err){
//     if(err) throw err;
//     console.log('deleted');
// });

//rename file
fs.rename('novel.txt', 'novel_kuda_laut.txt', function(err){
    if(err) throw err;
    console.log('renamed');
});