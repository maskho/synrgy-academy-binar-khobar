//product.controller.js
const Product = require('../models/product.model');

exports.test = function(request,response){
    response.send('Tes Controller OK!');
}
exports.product_create = function(request,response){
    let product = new Product({
        _id: request.body._id,
        nama: request.body.nama,
        jenis: request.body.jenis,
        deskripsi: request.body.deskripsi,
        penulis: request.body.penulis,
        penerbit: request.body.penerbit
    });
    product.save(function(err){
        if(err){
            return next(err);
        }
        response.send('buku berhasil dibuat');
    })
}
exports.product_details = function(request,response){
    Product.findById(request.params._id, function(err,product){
        if(err) return next(err);
        response.send(product);
    });
}
exports.product_update = function(request,response){
    Product.findByIdAndUpdate(request.params._id,{$set: request.body},function(err, product){
        if(err) return next(err);
        response.send('buku diupdate');
    });
}
exports.product_delete = function(request, response){
    Product.findByIdAndRemove(request.params._id, function(err){
        if(err) return next(err);
        response.send('Terhapus dan tak akan kembali');
    });
}