//product.model.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ProductSchema = new Schema({
    _id:{type: String, required:true},
    nama:{type: String, required:true, max:100},
    jenis:{type: String, required:true, max:100},
    deskripsi:{type: String, required:true, max:100},
    penulis:{type: String, required:true, max:100},
    penerbit:{type: String, required:true, max:100}
});

module.exports = mongoose.model('Product', ProductSchema);