//tokobuku.js
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://localhost:27017/tokobuku?readPreference=primary&appname=MongoDB%20Compass&ssl=false';
let mongoDB = process.env.MONGODB_URI || dev_db_url;

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connection Error'));

const product = require('./routes/product.route');
const tokobuku = express();

tokobuku.use(bodyParser.json());
tokobuku.use(bodyParser.urlencoded({extended:false}));
tokobuku.use('/buku', product);

let port = 1234;

tokobuku.listen(port,() => {
    console.log('Server hidup dan berjalan pada port '+port);
});