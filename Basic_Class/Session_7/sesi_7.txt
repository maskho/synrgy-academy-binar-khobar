﻿Review Development Environment
a. OS
Mengatur sistem perangkat keras dan perangkat lunak
Menggunakan Linux Ubuntu karena ini distro linux yang paling populer dan relatif mudah dipelajari. Menggunakan linux karena OS integrati dengan pengembangan aplikasi atau web
b. Code Editor
Menggunakan Visual Studio Code karena cukup ringan dan fitur lengkap dalam pengembangan web
c. Terminal
CLI bawaan ubuntu
interface untuk menulis baris2 perintah
d. Browser
menyajikan informasi
loading front end
Menggunakan firefox maupun chrome

Review additional Development Tools
a. Git GUI
git tanpa banyak ngetik ngoding. Menggunakan grafis

Review Git Workflow
a. Basic Git Command
git config untuk konfigurasi username emai;
init dari local clone untuk pertama kali mengintegrasikan remote repository dan local repository. Untuk update dari remote repo menggunakan pull, untuk update perubahan dari lokal ke remote menggunakan push. Alurnya add, commit, push
b. Push multi repository
ngepush di lebih dari satu repository
c. Resolve Conflict
menggunakan branching sesuai fitur
git stash untuk menyembunyikan kodingan jika lupa pull di awal
git stash pop atau git stash apply
d.Merge Request on Gitlab
untuk melakukan merge, menunjuk asignee untuk mereview kode

Review HTML Knowledge
a. Element
komponen/ obyek-obyek yang ada di HTML
b. Attribute
properti modifier element untuk menambah fungsionalitas dan karakteristik pada element
c. Temporary Editing Code on Browser
menggunakan inspect element untuk melihat kode frontend dan mengedit sementara layout

Review CSS Knowledge
a. Selector
menunjuk bagian element html yang distyling dalam blok kode css
b. Framework Overwrite
untuk override coding yang ada di bootstrap jika ingin melakukan styling lebih spesifik
c. Temporary editing code on browser
menggunakan inspect element untuk melihat kode frontend dan mengedit sementara styling layout

Review JavaScript knowledge
a. Variable
sebagai wadah dari value. Memiliki macam-macam jenis seperti string, int, float
b. ECMAScript
standarisasi kode javascript, yang terbaru ECMAScript6
c. Arithmetics
operasi aritmatika dalam pemrograman tambah kurang kali bagi remainder modulo pangkat dll.

