var dataSiswa = [
    {nama: 'Andi', nilai: 70},
    {nama: 'Ani', nilai: 78},
    {nama: 'Andrew', nilai: 89},
    {nama: 'Sam', nilai: 80},
    {nama: 'Witwicky', nilai: 75},
    {nama: 'Peter', nilai: 65},
    {nama: 'Parker', nilai: 70},
];
var nilaiTertinggi = 0;
var nilaiTerendah = 100;

function bandingTertinggi(x,y){
    if(x<y){
        return y;
    }else{
        return x;
    }
}
function bandingTerendah(x,y){
    if(x>y){
        return y;
    }else{
        return x;
    }
}

dataSiswa.forEach(item => {
    console.log('Nama: '+ item.nama);
    console.log('Nilai: '+ item.nilai);
    if(item.nilai>=70){
        console.log('Status: '+item.nama+' LULUS mapel');
    }else{
        console.log('Status: '+item.nama+' TIDAK LULUS mapel')
    }
    nilaiTertinggi = bandingTertinggi(nilaiTertinggi, item.nilai);
    nilaiTerendah = bandingTerendah(nilaiTerendah, item.nilai);
});

console.log('REKAP NILAI')
console.log('nilai tertinggi: '+nilaiTertinggi);
console.log('nilai terendah: '+nilaiTerendah);
