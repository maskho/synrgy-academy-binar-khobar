// ARITMATIKA
function penjumlahan(x,y){
    return x+y;
}
function pengurangan(x,y){
    return x-y;
}
function pembagian(x,y){
    return x/y;
}
perkalian = (x,y) => x*y;
modulus = (x,y) => x%y;

function tampil(nama,nilai){
    console.log('hasil dari fungsi '+ nama +' adalah '+ nilai);
}
tampil(penjumlahan.name, penjumlahan(1,2));   // 3
tampil(pengurangan.name, pengurangan(3,9));   // -6
tampil(pembagian.name, pembagian(1,2));     // 0.5
tampil(perkalian.name, perkalian(3,2));     // 6
tampil(modulus.name, modulus(5,2));       // 1

var person = [
    {nama: 'Saya', usia: 20, sisa: 14},
    {nama: 'Aku', usia: 49, sisa: 24},
    {nama: 'Gua', usia: 10, sisa: 49},
    {nama: 'Ane', usia: 22, sisa: 4},
    {nama: 'I', usia: 11, sisa: 34},
]
person.forEach(item =>{
    console.log('nama: '+ item.nama);
    console.log('usia: '+ item.usia);
    console.log('sisa usia: '+item.sisa);
    if(item.nama == 'Gua'){
        console.log('keren lu');
    }else{
        console.log('hilih');
    }
});
